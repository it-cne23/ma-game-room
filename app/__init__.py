from apiflask import APIFlask

from config import Config
from app.extensions import db

from app.models.object import Object

import flask_monitoringdashboard as dashboard


def create_room_objects():

    # Room Objects
    objects = [
        {
            'name': 'Wooden Desk', 
            'description': 'An old wooden desk with many drawers. One of the drawers has a key-hole in it.',
            'active':True,
            'icon':'archive-fill'
        },
        {
            'name': 'Column', 
            'description': 'Central to the room there is a stone column. The column has a three-digit combination lock on it.',
            'active':True,
            'icon':'database-lock'
        },
        {
            'name': 'Silver Key',
            'description': 'A worn key. You found it below the desk. The rests of the duct tape used to fix are still there.',
            'active':True,
            'icon':'key-fill'
        },
        {
            'name': 'Diary',
            'description': 'An old book with a bookmark inside. You wonder, what this great detective had to write.',
            'active':True,
            'icon':'journal'
        }
    ]

    # check if we already have objects in the DB
    cur_objs = Object.query.all()

    # if not create the objects initially
    if not cur_objs:
        for obj_data in objects:
            object = Object(**obj_data)
            db.session.add(object)
            db.session.commit()


def create_app(config_class=Config):
    app = APIFlask(__name__)
    app.config.from_object(config_class)

    # Flask Erweiterungen initialisieren
    db.init_app(app)
        
    # Blueprints registrieren
    from app.colosseum import bp as colosseum
    app.register_blueprint(colosseum, url_prefix='/colosseum')

    with app.app_context():
        db.create_all()
        create_room_objects()

    @app.route('/')
    def test_page():
        return {'message': 'Micro Adventures - Colosseum'}

    if Config.MONITORING_DASHBOARD_ENABLED:
        dashboard.config.database_name=Config.MONITORING_DATABASE_URL
        dashboard.config.table_prefix=Config.MONITORING_TABLE_PREFIX
        dashboard.bind(app)

    return app