from apiflask import APIBlueprint

bp = APIBlueprint('colosseum', __name__)

from app.colosseum import routes