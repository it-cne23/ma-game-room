from apiflask import Schema
from apiflask.fields import Integer, String, File
from apiflask.validators import FileSize, FileType

class DescriptionOut(Schema):
    description = String()

class PingOut(Schema):
    message = String()

class ExitsOut(Schema):
    north = String()
    east = String()
    south = String()
    west = String()

class TokenOut(Schema):
    token = String()

class InteractionIn(Schema):
    object_id = Integer(required=False)
    parameter = String(required=False)

class ImageOut(Schema):
    room_image = File(validate=[FileType(['.png', '.jpg', '.jpeg', '.gif']), FileSize(max='5 MB')])