
import pytest

@pytest.fixture
def auth_token(client):
    """Fixture to log in and return the authentication token."""
    response = client.post("/colosseum/start", json={'game_id': '11', 'player' : {'email': 'test@test.ch', 'name':'Test'}})
    token = response.json["token"]
    return token